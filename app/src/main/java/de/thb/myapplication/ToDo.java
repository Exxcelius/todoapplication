package de.thb.myapplication;

import androidx.room.*; //Vorrübergehend, geht vermutlich effizienter
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Entity(tableName = "todo")
@Data
@AllArgsConstructor
public class ToDo implements Comparable{

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "user")
    private User user;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "isFinished")
    private boolean isFinished;
    @ColumnInfo(name = "isImportant")
    private boolean isImportant;
    @ColumnInfo(name = "targetDateTime")
    private LocalDateTime targetDateTime;

    public ToDo(){
        this.title = "New ToDo";
        this.description = "";
        this.isFinished = false;
        this.isImportant = false;
        this.targetDateTime = LocalDateTime.now().plusDays(7);
        this.user = new User();
    }

    public ToDo(String title){
        this();
        this.title = title;
    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public int getId() { return id; }

    public User getUser() { return user; }

    public String getTitle() {
        return title;
    }

    //Room wants a setter for this field. Might be a problem however, since the id is supposed to be unique
    public void setId(int id) { this.id = id; }

    public void setUser(User user) { this.user = user; }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public LocalDateTime getTargetDateTime() {
        return targetDateTime;
    }

    public void setTargetDateTime(LocalDateTime targetDateTime) {
        this.targetDateTime = targetDateTime;
    }
}
