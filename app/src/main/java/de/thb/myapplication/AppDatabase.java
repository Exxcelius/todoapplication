package de.thb.myapplication;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@TypeConverters({Converters.class}) //TypeConverters required for LocalDateTime and User
@Database(entities = {ToDo.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    //Every Dao class, associated with the Database, goes here
    public abstract ToDoDao toDoDao();

    private static AppDatabase db;

    //Defacto Database Constructor, taken from https://github.com/android/architecture-components-samples/tree/30d783b4e64019aeadc8d082f1c23cf93c776f10/BasicSample
    public static AppDatabase getDatabase(final Context context){
        if(db == null){
            synchronized (AppDatabase.class){
                if(db == null){
                    db = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "todo")
                            //.addTypeConverter(new Converters())
                            .fallbackToDestructiveMigration().build();
                }
            }
        }
        return db;
    }
}
