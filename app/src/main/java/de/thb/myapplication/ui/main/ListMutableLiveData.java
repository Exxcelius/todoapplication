package de.thb.myapplication.ui.main;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.*;

public class ListMutableLiveData<T> extends MutableLiveData<ListHolder<T>> {

    public ListMutableLiveData(){
        Log.d("LiveData", "Created new Instance of ListMutableLiveData");
        this.setValue(new ListHolder<T>());
    }


    // this forces MutableLiveData to notify it's observers, by setting it's value without modifying it
    private void triggerNotify() {
        setValue(getValue());
    }

    public int size() {
        return this.getValue().size();
    }

    public T get(int index) {
        return this.getValue().get(index);
    }

    public List<T> getList(){
        return this.getValue().getList();
    }

    public void addAll(@NonNull Collection<T> items) {
        Log.d("Persistence","called ListLiveDate.addAll() with collection size " + items.size());
        if (items != null) {
            for (T item : items) {
                getValue().addItem(item);
            }
            triggerNotify();
        }
    }

    public void addItem(T item) {
        getValue().addItem(item);
        triggerNotify();
    }

    public void removeItem(T item) {
        getValue().removeItem(item);
        triggerNotify();
    }

    public void updateItem(T item) {
        getValue().updateItem(item);
        triggerNotify();
    }
    public UpdateType getUpdateType(){
        return getValue().getUpdateType();
    }
}

enum UpdateType {
    INSERT,
    UPDATE,
    DELETE
}

