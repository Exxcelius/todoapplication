package de.thb.myapplication.ui.main.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import de.thb.myapplication.ToDo;
import de.thb.myapplication.ui.main.ToDoViewModel;

import java.time.LocalDateTime;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) throws NullPointerException {
        ToDoViewModel model = new ViewModelProvider(requireActivity()).get(ToDoViewModel.class);
        ToDo selected = model.getSelected().getValue();
        LocalDateTime due;
        if (selected != null) {
            due = selected.getTargetDateTime();
        } else {
            throw new NullPointerException("Selected item was null");
        }

        return new TimePickerDialog(getActivity(), this, due.getHour(), due.getMinute(), true);

    }

    public void onTimeSet(TimePicker view, int hour, int minute) {
        Toast.makeText(getActivity(), "Confirmed Time", Toast.LENGTH_SHORT).show();

        ToDoViewModel model = new ViewModelProvider(requireActivity()).get(ToDoViewModel.class);
        ToDo selected = model.getSelected().getValue();
        LocalDateTime due;
        if (selected != null) {
            due = selected.getTargetDateTime();
        } else {
            throw new NullPointerException("Selected item was null");
        }
        selected.setTargetDateTime(LocalDateTime.of(due.getYear(), due.getMonthValue(), due.getDayOfMonth(), hour, minute, 0));
        model.updateItem(selected);
        model.setSelected(selected);
    }
}
