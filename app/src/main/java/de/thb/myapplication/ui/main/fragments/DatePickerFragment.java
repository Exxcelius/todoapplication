package de.thb.myapplication.ui.main.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import de.thb.myapplication.ToDo;
import de.thb.myapplication.ui.main.ToDoViewModel;

import java.time.LocalDateTime;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ToDoViewModel model = new ViewModelProvider(requireActivity()).get(ToDoViewModel.class);
        ToDo selected = model.getSelected().getValue();
        LocalDateTime due = selected.getTargetDateTime();

        return new DatePickerDialog(getActivity(), this, due.getYear(), due.getMonthValue(), due.getDayOfMonth());
    }

    public void onDateSet(DatePicker view, int year, int month, int day){
        ToDoViewModel model = new ViewModelProvider(requireActivity()).get(ToDoViewModel.class);
        ToDo selected = model.getSelected().getValue();
        selected.setTargetDateTime(LocalDateTime.of(year, month, day, selected.getTargetDateTime().getHour(), selected.getTargetDateTime().getMinute(), 0));
        model.updateItem(selected);

        Toast.makeText(getActivity(), "Confirmed Date", Toast.LENGTH_SHORT).show();

        // Show the TimePicker dialog afterwards
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getActivity().getSupportFragmentManager(), "timePicker");
    }
}
