package de.thb.myapplication.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.thb.myapplication.R;
import de.thb.myapplication.ToDo;

import java.util.Comparator;
import java.util.List;

/*
 * @Source https://guides.codepath.com/android/Using-an-ArrayAdapter-with-ListView
 *
 */
public class ToDoAdapter extends ArrayAdapter<ToDo> {

    private boolean sortImportant;

    public void sortList() {
        if (!sortImportant){
            this.sort(Comparator.comparing(ToDo::getTargetDateTime));
        }
        else{
            this.sort(((o1, o2) -> {
                if (o1.isImportant() == o2.isImportant()){
                    return o1.getTargetDateTime().compareTo(o2.getTargetDateTime());
                }
                else{
                    return o1.isImportant()?-1:1;
                }

            }));
        }
        notifyDataSetChanged();
    }

    public static class ViewHolder{
        TextView name;
        TextView date;
        CheckBox finished;
        CheckBox favorite;
        int index;

        public int getIndex(){
            return index;
        }
    }

    public ToDoAdapter(@NonNull Context context, int resource, @NonNull List<ToDo> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ToDo todo = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null){
            // if there's no view to reuse, inflate a new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.todo_item, parent, false);

            viewHolder.name = convertView.findViewById(R.id.todo_name);
            viewHolder.date = convertView.findViewById(R.id.finishDate);
            viewHolder.finished = convertView.findViewById(R.id.cb_isFinished);
            viewHolder.favorite = convertView.findViewById(R.id.cb_isFavorite);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(R.id.to_do_view_holder_id, viewHolder);
        }
        else{
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag(R.id.to_do_view_holder_id);
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        assert todo != null;
        viewHolder.name.setText(todo.getTitle());
        viewHolder.date.setText(todo.getTargetDateTime().toString());
        viewHolder.finished.setChecked(todo.isFinished());
        viewHolder.favorite.setChecked(todo.isImportant());
        viewHolder.index = position;

        // color code overdue ToDos
        /*if (todo.getTargetDateTime().isBefore(LocalDateTime.now())){
            if (todo.isFinished()){
                convertView.setBackgroundColor(R.color.colorOverdueFinished);
            }
            else{
                convertView.setBackgroundColor(R.color.colorOverdueUnfinished);
                viewHolder.name.setTextColor(R.color.colorTextOverdue);
            }
        }
        else{
            convertView.setBackgroundColor(R.color.colorPrimary);
            viewHolder.name.setTextColor(R.color.colorAccent);
        }*/

        // Return the completed View to Render on Screen
        return convertView;
    }

    @Nullable
    @Override
    public ToDo getItem(int position) {
        return super.getItem(position);
    }

    public void setSortImportant(boolean important){
        this.sortImportant = important;
    }
}
