package de.thb.myapplication.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.ViewModelProvider;
import de.thb.myapplication.R;
import de.thb.myapplication.ToDo;
import de.thb.myapplication.ui.main.ToDoViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ToDoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ToDoDetailFragment extends Fragment {

    private ToDoViewModel model;

    public ToDoDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ToDoDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ToDoDetailFragment newInstance(ToDoViewModel model) {

        ToDoDetailFragment fragment = new ToDoDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.model = model;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(ToDoViewModel.class);

        if (getArguments() != null) {
            // if arguments exist, handle them here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.todo_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set Action Listener for Description
        EditText editDescription = (EditText) getView().findViewById(R.id.todo_edit_description);
        editDescription.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                handled = true;
                handleDescription(editDescription);

            }
            return handled;
        });
        editDescription.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                handleDescription(editDescription);
            }
        });

        EditText editTitle = (EditText) getView().findViewById(R.id.todo_edit_name);
        editTitle.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                //handled = true;
                String newTitle = ((EditText) getView().findViewById(R.id.todo_edit_name)).getText().toString();
                if (!newTitle.isEmpty()) {
                    handled = true;
                    handleTitle(newTitle, editTitle, editDescription);
                } else {
                    Toast.makeText(getActivity(), R.string.error_empty_title, Toast.LENGTH_SHORT).show();
                    editTitle.requestFocus();
                }
            }
            return handled;
        });
        editTitle.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String newTitle = ((EditText) getView().findViewById(R.id.todo_edit_name)).getText().toString();
                if (!newTitle.isEmpty()) {
                    handleTitle(newTitle, editTitle, editDescription);
                } else {
                    Toast.makeText(getActivity(), R.string.error_empty_title, Toast.LENGTH_SHORT).show();

                }
            }
        });

        model.getSelected().observe(requireActivity(), todo -> {
            updateView(todo);
        });
    }

    private void handleTitle(String newTitle, EditText editTitle, EditText editDescription) {
        model.getSelected().getValue().setTitle(newTitle);
        editTitle.clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editDescription.getWindowToken(), 0);
        editTitle.setVisibility(View.GONE);
        TextView title = (TextView) getView().findViewById(R.id.todo_name);
        title.setText(model.getSelected().getValue().getTitle());
        title.setVisibility(View.VISIBLE);
        model.getListLiveData().updateItem(model.getSelected().getValue());
    }

    private void handleDescription(EditText editDescription) {
        model.getSelected().getValue().setDescription(((EditText) getView().findViewById(R.id.todo_edit_description)).getText().toString());
        editDescription.clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editDescription.getWindowToken(), 0);
        editDescription.setVisibility(View.GONE);
        TextView description = (TextView) getView().findViewById(R.id.todo_description);
        description.setText(model.getSelected().getValue().getDescription());
        description.setVisibility(View.VISIBLE);
        model.updateItem(model.getSelected().getValue());
    }

    public void updateView(ToDo clicked) {
        Log.d("DetailView", "DetailView updated");
        if (this.getView() != null) {
            ((TextView) this.getView().findViewById(R.id.todo_name)).setText(clicked.getTitle());
            ((TextView) this.getView().findViewById(R.id.todo_description)).setText(clicked.getDescription());
            ((CheckBox) this.getView().findViewById(R.id.todo_finished)).setChecked(clicked.isFinished());
            ((CheckBox) this.getView().findViewById(R.id.todo_important)).setChecked(clicked.isImportant());
            ((TextView) this.getView().findViewById(R.id.todo_due)).setText(clicked.getTargetDateTime().toString());
        }
    }
}