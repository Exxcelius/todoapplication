package de.thb.myapplication.ui.main.fragments;

import android.os.Bundle;
import android.widget.ListView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.thb.myapplication.R;
import de.thb.myapplication.ui.main.ToDoAdapter;
import de.thb.myapplication.ui.main.ToDoViewModel;

/**
 * A fragment representing a list of Items.
 */
public class ToDoListFragment extends Fragment {

    private ToDoViewModel model;
    private ToDoAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ToDoListFragment() {}

    public static ToDoListFragment newInstance(ToDoViewModel model, ToDoAdapter adapter){
        ToDoListFragment fragment = new ToDoListFragment();
        fragment.adapter =adapter;
        fragment.model = model;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this).get(ToDoViewModel.class);
        //todos = mViewModel.getTodoList().getValue();
        if (getArguments() != null) {
            // if args are needed, handle them here
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do_list, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listView = getView().findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}