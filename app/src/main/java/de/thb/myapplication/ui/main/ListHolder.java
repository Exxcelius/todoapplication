package de.thb.myapplication.ui.main;

import java.util.ArrayList;
import java.util.List;

public class ListHolder<T> {

    private final List<T> list;
    private T itemChanged = null;
    UpdateType updateType = null;

    public ListHolder(List<T> list) {
        this.list = list;
    }

    public ListHolder() {
        this.list = new ArrayList<T>();
    }

    public int size() {
        return this.list.size();
    }

    public T get(int index) {
        try {
            return this.list.get(index);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<T> getList() {
        return this.list;
    }

    public void addItem(T item) {
        this.list.add(item);
        this.itemChanged = item;
        this.updateType = UpdateType.INSERT;
    }

    public void removeItem(T item) {
        this.itemChanged = item;
        this.updateType = UpdateType.DELETE;
    }

    public void updateItem(T item) {
        this.itemChanged = item;
        this.updateType = UpdateType.UPDATE;
    }

    public UpdateType getUpdateType() {
        return updateType;
    }

    public T getItemChanged() {
        return itemChanged;
    }
}
