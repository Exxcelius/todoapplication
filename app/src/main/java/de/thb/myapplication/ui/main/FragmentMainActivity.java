package de.thb.myapplication.ui.main;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import de.thb.myapplication.R;
import de.thb.myapplication.ToDo;
import de.thb.myapplication.ui.main.fragments.DatePickerFragment;
import de.thb.myapplication.ui.main.fragments.ToDoDetailFragment;
import de.thb.myapplication.ui.main.fragments.ToDoListFragment;

import java.util.List;

public class FragmentMainActivity extends AppCompatActivity {

    private ToDoViewModel model;
    private List<ToDo> todos;
    private boolean tabletMode = false;
    private boolean showsDetails = false;
    private boolean sortImportant = false;

    private ToDoListFragment mListFragment;
    private ToDoDetailFragment mDetailFragment;
    private ToDoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        model = new ViewModelProvider(this).get(ToDoViewModel.class);
        todos = model.getListLiveData().getList();
        adapter = new ToDoAdapter(this, R.id.list, todos);
        mListFragment = ToDoListFragment.newInstance(model, adapter);
        mDetailFragment = ToDoDetailFragment.newInstance(model);

        setContentView(R.layout.fragment_main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mListFragment)
                    .commitNow();
        }

        model.getSelected().observe(this, mDetailFragment::updateView);
        model.getListLiveData().observe(this, listHolder ->
        {
            Log.d("Persistence", "model items:" + model.getListLiveData().getList().size());
            this.adapter.notifyDataSetChanged();

            /*
            TODO overwork with viewmodel
            if (listHolder.getUpdateType() != null) {

                switch (listHolder.getUpdateType()) {
                    case INSERT:
                        model.addItem(listHolder.getItemChanged());
                        break;
                    case DELETE:
                        model.deleteItem(listHolder.getItemChanged());
                        break;
                    case UPDATE:
                        model.updateItem(listHolder.getItemChanged());
                        break;
                    default:
                        break;
                }
            }*/

        });


    }

    @Override
    public void onBackPressed() {
        if (!tabletMode && showsDetails) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mListFragment)
                    .commitNow();
            showsDetails = false;
        } else {
            super.onBackPressed();
        }
    }

    public void onItemClick(View view) {
        Log.d("DetailView", "Item clicked, working on DetailView");
        ToDo clicked = getToDoByView(view);

        if (tabletMode) {
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mDetailFragment)
                    .commitNow();
            showsDetails = true;
        }
        model.setSelected(clicked);
        Toast.makeText(this, "Clicked item " + clicked.getTitle(), Toast.LENGTH_SHORT).show();
    }


    private ToDo getToDoByView(View view) {
        while (true) {
            Object tag = view.getTag(R.id.to_do_view_holder_id);
            if (tag != null) {
                int index = ((ToDoAdapter.ViewHolder) tag).getIndex();
                return this.todos.get(index);
            }

            if (view.getParent() == null) {
                break;
            } else {
                view = (View) view.getParent();
            }
        }
        return null;
    }

    public void onIsFinishedClicked(View view) {
        ToDo clicked = getToDoByView(view);
        clicked.setFinished(!clicked.isFinished());

        //Toast.makeText(this, "OnClick", Toast.LENGTH_SHORT).show();
    }

    public void onIsFavoriteClicked(View view) {
        ToDo clicked = getToDoByView(view);
        clicked.setImportant(!clicked.isImportant());
        adapter.sortList();
        //Toast.makeText(this, "OnClick", Toast.LENGTH_SHORT).show();
    }

    public void onDetailFinishedClicked(View view) {
        ToDo selected = model.getSelected().getValue();
        selected.setFinished(!selected.isFinished());
        model.getListLiveData().updateItem(selected);
    }

    public void onDetailImportantClicked(View view) {
        ToDo selected = model.getSelected().getValue();
        selected.setImportant(!selected.isImportant());
        model.getListLiveData().updateItem(selected);
    }

    public void onDescriptionClicked(View view) {
        ToDo selected = model.getSelected().getValue();
        TextView description = findViewById(R.id.todo_description);
        EditText editDescription = findViewById(R.id.todo_edit_description);

        editDescription.setText(selected.getDescription());
        description.setVisibility(View.GONE);
        editDescription.setVisibility(View.VISIBLE);
        editDescription.requestFocus();
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editDescription, 0);
    }

    public void onTitleClicked(View view){
        ToDo selected = model.getSelected().getValue();
        TextView title = findViewById(R.id.todo_name);
        EditText editTitle = findViewById(R.id.todo_edit_name);

        editTitle.setText(selected.getTitle());
        title.setVisibility(View.GONE);
        editTitle.setVisibility(View.VISIBLE);
        editTitle.requestFocus();
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTitle, 0);
    }

    public void onDueClicked(View view){
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void onDeleteClicked(View view){
        new AlertDialog.Builder(this)
                .setTitle(R.string.warning_delete)
                .setMessage(R.string.warning_delete_message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, ((dialog, which) -> {
                    model.deleteItem(model.getSelected().getValue());
                    if (!tabletMode && showsDetails) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, mListFragment)
                                .commitNow();
                        showsDetails = false;}
                    else{
                        // TODO: this is gonna be annoying
                    }
                }))
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void onCreateClicked(View view){
        Toast.makeText(this, "Create new", Toast.LENGTH_SHORT).show();
        ToDo created = new ToDo();
        model.addItem(created);
        model.setSelected(created);
        if (!tabletMode && !showsDetails){
            showsDetails = true;
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mDetailFragment)
                    .commitNow();
        }
    }

    public void onSortClicked(View view){
        Toast.makeText(this, "Sort the list", Toast.LENGTH_SHORT).show();
        sortImportant = !sortImportant;
        Button sorter = findViewById(R.id.btn_sort);
        if (sortImportant){
            sorter.setText(R.string.label_sort_important);
        }
        else{
            sorter.setText(R.string.label_sort_date);
        }
        this.adapter.setSortImportant(this.sortImportant);
        this.adapter.sortList();
    }
}