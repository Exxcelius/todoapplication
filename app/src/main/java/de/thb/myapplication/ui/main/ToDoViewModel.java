package de.thb.myapplication.ui.main;


import android.app.Application;
import android.util.Log;
import androidx.lifecycle.*;
import de.thb.myapplication.ToDo;
import de.thb.myapplication.ToDoRepository;

import java.util.List;
import java.util.Observable;

public class ToDoViewModel extends AndroidViewModel {
    private ListMutableLiveData<ToDo> todoList;
    private MutableLiveData<ToDo> selected;
    private ToDoRepository toDoRepo;
    private Application application;

    public ToDoViewModel(Application application) {
        super(application);
        this.application = application;
        toDoRepo = ToDoRepository.getInstance(application);
    }

    public ListMutableLiveData<ToDo> getListLiveData() {
        Log.d("Persistence", "getListLiveData called");
        if (todoList == null) {
            Log.d("Persistence", "ListLiveData is null");
            todoList = new ListMutableLiveData<ToDo>();
            loadTodos();
        }
        return this.todoList;
    }

    public LiveData<ToDo> getSelected() {
        Log.d("DetailView", "selected requested");
        if (this.selected == null) {
            this.selected = new MutableLiveData<ToDo>();
        }
        return this.selected;
    }

    public void setSelected(ToDo selected) {
        Log.d("DetailView", "set selected " + selected.getTitle());
        if (this.selected == null) {
            this.selected = new MutableLiveData<ToDo>();
        }
        this.selected.setValue(selected);
    }

    private void loadTodos() {
        Log.d("Persistence", "Loading todos");
        boolean hasLoaded = false;
        LiveData<List<ToDo>> data = toDoRepo.getAll();


        Observer<List<ToDo>> observer = new Observer<List<ToDo>>() {
            private boolean hasLoaded = false;

            @Override
            public void onChanged(List<ToDo> toDos) {
                if (!hasLoaded) {
                    hasLoaded = true;
                    todoList.addAll(toDos);
                    Log.d("Persistence", "In callback" + data.getValue().size());
                }
            }
        };
        data.observeForever(observer);

    }

    // CRUD operations
    public void addItem(ToDo item) {
        //this.todoList.getList().add(item);
        this.todoList.addItem(item);
        this.toDoRepo.addItem(item);
    }

    public void updateItem(ToDo item) {
        this.todoList.updateItem(item);
        this.toDoRepo.updateItem(item);
    }

    public void deleteItem(ToDo item) {
        this.todoList.getList().remove(item);
        this.todoList.removeItem(item);
        this.toDoRepo.deleteItem(item);
    }


}
