package de.thb.myapplication;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import de.thb.myapplication.ui.main.ListMutableLiveData;
//import de.thb.myapplication.ui.main.ListMutableLiveData;

import java.time.LocalDateTime;
import java.util.List;

@Dao
public interface ToDoDao {

    @Query("SELECT * FROM todo")
    LiveData<List<ToDo>> getAll();

    //I don't really know, how useful this would be to the project
    //@Query("SELECT * FROM todo WHERE id IN (:ids)")
    //List<ToDo> loadAllByIds(long[] ids);

    @Query("SELECT * FROM todo WHERE id = :id LIMIT 1")
    ToDo findById(int id);

    @Query("SELECT * FROM todo WHERE user LIKE :user")
    ToDo findByUser(String user);


    // -------- Region start -------------------
    // these are probably unnecessary, we will always need all todos of a user
    // When needed, this logic can be done on Runtime
    @Query("SELECT * FROM todo WHERE isFinished LIKE :isFinished")
    ToDo findByIsFinished(boolean isFinished);

    //Finds all ToDos, which are still active. Hypothetically
    @Query("SELECT * FROM todo WHERE isFinished NOT LIKE :isFinished AND :currentDateTime > :targetDateTime")
    ToDo findByIsNotFinished(boolean isFinished, LocalDateTime currentDateTime, LocalDateTime targetDateTime);

    @Query("SELECT * FROM todo WHERE isImportant LIKE :isImportant")
    ToDo findByIsImportant(boolean isImportant);

    // --------- End region -------------

    @Update
    void updateToDo(ToDo todo);

    @Insert
    void insertToDo(ToDo todo);

    @Insert
    void insertToDo(ToDo... todos);

    @Insert
    public void insertAllToDos(List<ToDo> todo);

    @Delete
    void deleteToDo(ToDo todo);

    @Delete
    void deleteToDo(ToDo... todos);

    @Delete
    void deleteToDos(List<ToDo> todo);
}