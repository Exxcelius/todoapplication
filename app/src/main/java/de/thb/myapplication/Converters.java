package de.thb.myapplication;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import java.time.LocalDateTime;

//Room can't persist complex types, so we need TypeConverters

public class Converters {

    //Greatly inspired by https://developer.android.com/training/data-storage/room/referencing-data
    //I should note, that this might not be 100% backwards compatible
    @TypeConverter
    public static LocalDateTime toDate(String localDateString){
        return localDateString == null ? null : LocalDateTime.parse(localDateString);
    }

    @TypeConverter
    public static String toDateString(LocalDateTime localDate){
        return localDate == null ? null : localDate.toString();
    }

    //No idea what I am supposed to do with User rn, so fuck it, placeholder
    @TypeConverter
    public static User toUser(int intStamp){
        return intStamp == 0 ? null : new User();
    }

    @TypeConverter
    public static int toInt(User user){
        return user == null ? null : user.getInt();
    }

}

