package de.thb.myapplication;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import de.thb.myapplication.ui.main.ListMutableLiveData;

import java.util.List;

public class ToDoRepository {

    private static ToDoRepository mInstance;
    private final ToDoDao toDoDao;
    private LiveData<List<ToDo>> toDoList;
    private Application application;

    public static ToDoRepository getInstance(Application application) {
        return mInstance == null ? new ToDoRepository(application) : mInstance;
    }

    private ToDoRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        this.application = application;
        //Do this later when we got it to start

        toDoDao = db.toDoDao();
    }

    public LiveData<List<ToDo>> getAll() {
        if (toDoList == null) {
            toDoList = toDoDao.getAll();
        }
        return toDoList;
    }

    public void addItem(ToDo item) {
        Log.d("Persistance", "Repo is saving");
        new Thread((Runnable) () -> {
            toDoDao.insertToDo(item);
            Log.d("Persistence", "Saved item " + item.toString());
        }).start();
    }

    public void updateItem(ToDo item){
        new Thread((Runnable) ()->{
            toDoDao.updateToDo(item);
        }).start();
    }

    public void deleteItem(ToDo item){
        new Thread((Runnable) ()->{
            toDoDao.deleteToDo(item);
        }).start();
    }
}
